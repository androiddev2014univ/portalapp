LOCAL_PATH := $(call my-dir)

# -------------------------------------------------------------------------------------------
include $(CLEAR_VARS)
LOCAL_MODULE    := libdatabase_sqlcipher
LOCAL_SRC_FILES := external/$(TARGET_ARCH_ABI)/$(LOCAL_MODULE)$(TARGET_SONAME_EXTENSION)
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE    := libsqlcipher_android
LOCAL_SRC_FILES := external/$(TARGET_ARCH_ABI)/$(LOCAL_MODULE)$(TARGET_SONAME_EXTENSION)
include $(PREBUILT_SHARED_LIBRARY)

#include $(CLEAR_VARS)
#LOCAL_MODULE    := libstlport_shared
#LOCAL_SRC_FILES := external/$(TARGET_ARCH_ABI)/$(LOCAL_MODULE)$(TARGET_SONAME_EXTENSION)
#include $(PREBUILT_SHARED_LIBRARY)


# ///////////////////////////////////////////////////////////////////////////////////////////
include $(CLEAR_VARS)
LOCAL_MODULE    := Portalapp
LOCAL_SRC_FILES := Portalapp.cpp
include $(BUILD_SHARED_LIBRARY)

