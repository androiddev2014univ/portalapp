package com.example.portalapp.library;

import android.app.AlarmManager;
import android.content.Context;

/*
 * Androidに実行するサービスの時間を設定・取り消しをするクラス
 */
public class Plan {
	Context context;
	String SharedPreferenceFileName;
	String isNotifiTagKey; String intervalTagKey; long Interval_UK2JP;
	public Plan(Context context, String shared_preferences_file_name,
			String isNotifiTagKey, String intervalTagKey, long Interval_UK2JP){
		this.context = context;
		SharedPreferenceFileName = shared_preferences_file_name;
		this.isNotifiTagKey = isNotifiTagKey;
		this.intervalTagKey = intervalTagKey;
		this.Interval_UK2JP = Interval_UK2JP;
	}
	public void refreshSchedule(){
		SP sp = new SP(SharedPreferenceFileName, this.context);
		final boolean isNotifi = sp.readBool(isNotifiTagKey);
		
		if(isNotifi){
			this.setSchedule();
			
		}else{
    		this.cancelSchedule();
    		
		}
	}
	public void setSchedule(){
		SP sp = new SP(SharedPreferenceFileName, this.context);
		final int sig = sp.readInt(intervalTagKey);
		long interval_time;
		switch(sig){
			case 0: interval_time = AlarmManager.INTERVAL_FIFTEEN_MINUTES / 3; break;
			case 1: interval_time = AlarmManager.INTERVAL_FIFTEEN_MINUTES; break;
			case 2: interval_time = AlarmManager.INTERVAL_HALF_HOUR; break;
			case 3: interval_time = AlarmManager.INTERVAL_HOUR; break;
			case 4: interval_time = AlarmManager.INTERVAL_HALF_DAY / 2; break;
			case 5: interval_time = AlarmManager.INTERVAL_HALF_DAY; break;
			case 6: interval_time = AlarmManager.INTERVAL_DAY; break;
			case 7: interval_time = AlarmManager.INTERVAL_DAY * 3; break;
			default: interval_time = AlarmManager.INTERVAL_DAY; break;
		}
		TimeObject time = new TimeObject(System.currentTimeMillis() + interval_time + Interval_UK2JP);
		DailyScheduler scheduler = new DailyScheduler(this.context);
		scheduler.setByTime(com.example.portalapp.serviceBackground.CheckLatestMessage.class,
				time.gethour(), time.getminuite(), time.getsecond(), 0, interval_time);
		
		//Toast.makeText(this, "スケジュールをセット", Toast.LENGTH_LONG).show();
	}
	public void cancelSchedule(){
		DailyScheduler scheduler = new DailyScheduler(this.context);
		scheduler.cancel(com.example.portalapp.serviceBackground.CheckLatestMessage.class, 0, 0);
		
		//Toast.makeText(this, "スケジュールをキャンセル", Toast.LENGTH_LONG).show();
	}

}
