package com.example.portalapp.library;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.widget.Toast;

public class Access {
	private Context context = null;
	public Access (Context context){
		this.context = context;
	}
	/*
     * 参考URL：http://androidstudio.hatenablog.com/entry/2014/07/24/173630
     */
	public String getAssetsFile(String buf, String filePath){
		InputStream is = null;
		BufferedReader br = null;
		try {
		    try {
		        // assetsフォルダ内の"filename"をオープンする
		        is = this.context.getAssets().open(filePath);
		        br = new BufferedReader(new InputStreamReader(is));

		        // １行ずつ読み込み、改行を付加する
		        String str;
		        while ((str = br.readLine()) != null) {
		        	buf += str + "\n";
		        }
		    } finally {
		        if (is != null) is.close();
		        if (br != null) br.close();
		    }
		} catch (Exception e){
		    // エラー発生時の処理
			Toast.makeText(this.context, "読み込みエラー："+e.toString(), Toast.LENGTH_SHORT).show();
		}
		return buf;
	}
	
	/*
	 * 参考URL：http://qiita.com/kuniorock/items/25c885fe7e60a2273395
	 */
	/**
	 * バージョンコードを取得
	 * @return
	 */
	public final int getVersionCode(){
		int code;
		try{
			code = getPakageInfo(this.context).versionCode;
		}catch(Exception e){
			e.printStackTrace();
			code = 0;
		}
		return code;
	}

	/*
	 * 参考URL：http://qiita.com/kuniorock/items/25c885fe7e60a2273395
	 */
	/**
	 * バージョン名を取得
	 * @return
	 */
	public final String getVersionName(){
		String name;
		try{
			name = getPakageInfo(this.context).versionName;
		}catch(Exception e){
			e.printStackTrace();
			name = "";
		}
		return name;
	}
	
	// ___________________________________________________________________________________
	private PackageInfo getPakageInfo(Context context) throws NameNotFoundException{
		return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
	}
	
}
