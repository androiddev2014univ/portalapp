/**
 * 
 */
package com.example.portalapp.bustime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.example.portalapp.bustime.BusRoute.Time;


public class LoadBusTime {
    private final String KEIHANBUS_SCRIPT_URL;
    private String[] urls;
    private TreeMap<Integer, BusTimeTable> tables;

    private TreeMap<String, String> nagaoToUnivBusTime;
    private TreeMap<String, String> univToNagaoBusTime;

    private TreeMap<String, String> keihanToUnivBusTime;
    private TreeMap<String, String> univToKeihanBusTime;

    private ArrayList<ArrayList<Integer>> routesIndexes;
    
    public LoadBusTime(String KEIHANBUS_SCRIPT_URL, String[] urls) {
        this.KEIHANBUS_SCRIPT_URL = KEIHANBUS_SCRIPT_URL;
        this.urls = urls;
        
        routesIndexes = new ArrayList<ArrayList<Integer>>();
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(1, 2)));
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(1, 2)));
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(0, 1)));
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(1)));
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(2)));
        routesIndexes.add(new ArrayList<Integer>(Arrays.asList(1)));
        

        nagaoToUnivBusTime = new TreeMap<String, String>();
        univToNagaoBusTime = new TreeMap<String, String>();
        keihanToUnivBusTime = new TreeMap<String, String>();
        univToKeihanBusTime = new TreeMap<String, String>();
        
        tables = new TreeMap<Integer, BusTimeTable>();
        


		LoadThread th = new LoadThread();
		th.start();
		try {
			th.join();
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
        

        for(int i=0;i<5;i++) {
            storeBusTime(i);
        }
        

    }
    
    private class LoadThread extends Thread{
    	@Override
    	public void run(){
            for(int i=0;i<5;i++) {
                load(i);
            }
            
    	}
    	
    }
    
    private void storeBusTime(int index) {

        for (int h = 0; h <= 24; h++) {
            for (Integer routeIndex : routesIndexes.get(index)) {

                BusRoute busRoute = tables.get(index).getRoute(routeIndex.intValue());
                Time time = busRoute.getTime(h);
                Time.Minute[] minutes = time.minutes;

                if (minutes == null) {
                    continue;
                }

                for (int i = 0; i < minutes.length; i++) {
                    String key = String.format("%02d:%02d", h, minutes[i].intValue());
                    String val = busRoute.getDest().replaceAll("¥n", " ");
                    
                    if(index == 0) {
                        nagaoToUnivBusTime.put(key, val);
                    }
                    else if(index == 1) {
                        univToNagaoBusTime.put(key, val);
                    }
                    else if(index == 2) {
                        keihanToUnivBusTime.put(key, val);
                    }
                    else {
                        univToKeihanBusTime.put(key, val);
                    }
                }
            }
        }
    }

    private void load(final int index) {
        try {
            Document doc = Jsoup.connect(KEIHANBUS_SCRIPT_URL + urls[index]).get();
            Element htmlTable = doc.getElementById("weekday");
            KeihanBusTimeTableParser parser = new KeihanBusTimeTableParser();
            BusTimeTable table = parser.parse(htmlTable);

            addTables(index, table);
            
        } catch (IOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }
                
    }
    
    private synchronized void addTables(int index, BusTimeTable table) {
        tables.put(index, table);
    }

    public TreeMap<String, String> getNagaoToUnivBusTime() {
        return this.nagaoToUnivBusTime;
    }

    public TreeMap<String, String> getUnivToNagaoBusTime() {
        return this.univToNagaoBusTime;
    }

    public TreeMap<String, String> getKeihanToUnivBusTime() {
        return this.keihanToUnivBusTime;
    }

    public TreeMap<String, String> getUnivToKeihanBusTime() {
        return this.univToKeihanBusTime;
    }
    
}
