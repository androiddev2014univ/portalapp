package com.example.portalapp.bustime;

public class BusTimeTable {
    String caption;// 平日、土曜、休日
    BusRoute[] routes = null;

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getCaption() {
        return caption;
    }

    public void alloc(int numRoutes) {
        routes = new BusRoute[numRoutes];
    }

    public void setRoute(int idx, BusRoute route)
            throws ArrayIndexOutOfBoundsException {
        routes[idx] = route;
    }

    public void setRoutes(BusRoute[] routes, int size) {
        if (routes == null) {
            this.routes = null;
        }
        System.arraycopy(routes, 0, this.routes, 0, size);
    }

    public void setRoutes(BusRoute[] routes) {
        if (routes == null) {
            this.routes = null;
        }
        this.routes = routes.clone();
    }

    public BusRoute getRoute(int idx) throws ArrayIndexOutOfBoundsException {
        return routes[idx];
    }

    public int getRouteSize() {
        if (routes == null) {
            return 0;
        }
        return routes.length;
    }

    public String toString() {
        String s = this.getCaption() + "¥n";
        for (int i = 0; i < getRouteSize(); ++i) {
            s += getRoute(i).toString();
        }
        return s;
    }

    public BusRoute[] getRouteByDest(String dest) {
        return null;
    }

    public BusRoute[] getRouteByID(String routeID) {
        return null;
    }
}
