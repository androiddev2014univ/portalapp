package com.example.portalapp.bustime;

public class BusRoute {
    /* ArrayList 等使わずに、できるだけ普通の配列を使用しています */
    /* 必要に応じて修正してください */
    
    public static class Time {
        public static class Minute {
            public String sign = "";// "牧"、"▲"
            public int minute;

            public void set(String sign, int minute) {
                this.sign = sign;
                this.minute = minute;
            }

            public void set(int minute) {
                this.sign = "";
                this.minute = minute;
            }

            public void set(String signAndMinute) {
                boolean found = false;
                int i = 0;
                for (i = 0; i < signAndMinute.length(); ++i) {
                    found = Character.isDigit(signAndMinute.charAt(i));
                    if (found) {
                        break;
                    }
                }
                if (found == false) {
                    return;
                }
                if (i == 0) {
                    set(Integer.parseInt(signAndMinute));
                    return;
                }
                set(signAndMinute.substring(0, i),
                        Integer.parseInt(signAndMinute.substring(i)));
            }

            public String toString() {
                return sign + minute;
            }
            
            public int intValue() {
                return minute;
            }
        }

        int hour;
        // 外部から参照したかったためpublicに変更しました misaki(2015/1/22)
        public Minute[] minutes = null;

        public Time(int hour) {
            this.hour = hour;
        }

        public void alloc(int numMinutes) {
            minutes = new Minute[numMinutes];
            for (int i = 0; i < minutes.length; ++i) {
                minutes[i] = new Minute();
            }
        }

        public int getMinuteSize() {
            if (minutes == null) {
                return 0;
            }
            return minutes.length;
        }

        public void setMinutes(Minute[] minutes) {
            if (minutes == null) {
                this.minutes = null;
            } else {
                this.minutes = minutes.clone();
            }
        }            

        public void setMinutes(String[] data) {
            if (data == null) {
                this.minutes = null;
                return;
            }
            if (data.length == 0) {
                this.minutes = null;
                return;
            }
            alloc(data.length);
            for (int i = 0; i < data.length && i < minutes.length; ++i) {
                getMinutes(i).set(data[i]);
            }
        }

        public int getHour() {
            return this.hour;
        }

        public Minute getMinutes(int idx) throws ArrayIndexOutOfBoundsException {
            return minutes[idx];
        }

        public String toString() {
            String s = getHour() + " 時¥n";
            if (this.getMinuteSize() == 0) {
                return s + " なし";
            }
            s += " " + this.getMinutes(0).toString();
            for (int i = 1; i < this.getMinuteSize(); ++i) {
                s += ", " + this.getMinutes(i).toString();
            }
            s += " 分¥n";
            return s;
        }
    }

    String routeID;
    String dest;
    Time[] times = new Time[25];// 0 - 24

    public BusRoute() {
        for (int i = 0; i < times.length; ++i) {
            times[i] = new Time(i);
        }
    }

    private boolean isValidString(String s) {
        if (s == null) {
            return false;
        } else if (s.length() == 0) {
            return false;
        }
        return true;
    }

    public boolean isValid() {
        return isValidString(getRouteID()) && isValidString(getDest());
    }

    public void setRouteID(String id) {
        routeID = id;
    }

    public String getRouteID() {
        return routeID;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getDest() {
        return dest;
    }

    public Time getTime(int h) throws ArrayIndexOutOfBoundsException {
        return times[h];
    }

    public String toString() {
        String s = "*-----*¥n";
        s += "（系統番号）¥n" + getRouteID() + "¥n";
        s += "（行先）¥n" + getDest() + "¥n";
        s += "（時刻）¥n";
        for (int i = 0; i < times.length; ++i) {
            if (getTime(i).getMinuteSize() == 0) {
                continue;
            }
            s += getTime(i).toString();
        }
        return s;
    }
}
