package com.example.portalapp.bustime;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

public class KeihanBusTimeTableParser {
    public int[] parseBusTimeHours(Element elem) {
        int[] hours = null;
        int[] tmp = new int[24];
        int numHours = 0;
        Elements cells = elem.select("td");
        for (int i = 0; i < cells.size() && i < tmp.length; ++i) {
            int h = -1;
            String text = cells.get(i).text();
            if (text.isEmpty()) {
                continue;
            }
            try {
                h = Integer.parseInt(text);
            } catch (NumberFormatException e) {
                // Log.d("Info", text + " は数字ではない");
            }
            if (h >= 0) {
                tmp[numHours++] = h;
            }
        }
        if (numHours == 0) {
            // Log.d("Error", "時間データが０個");
            return null;
        }
        hours = new int[numHours];
        System.arraycopy(tmp, 0, hours, 0, numHours);
        return hours;
    }

    public String[] parseCell(Element cell) {
        List<TextNode> texts = cell.textNodes();
        if (texts.isEmpty()) {
            return null;
        }
        int size = texts.size();
        String[] data = new String[size];
        for (int i = 0; i < size; ++i) {
            data[i] = texts.get(i).text();
        }
        return data;
    }

    public String parseAndCatCell(Element cell) {
        String[] data = parseCell(cell);
        if (data.length == 0) {
            return null;
        }
        String ret = data[0];
        for (int i = 1; i < data.length; ++i) {
            ret += "¥n";
            ret += data[i];
        }
        return ret;
    }

    public BusRoute parseRoute(Element elem, int[] hours) {
        BusRoute route = new BusRoute();
        Elements cells = elem.select("td");
        if (cells.isEmpty()) {
            return null;
        }
        int hIndex = 0;
        int i = 0;
        for (i = 0; i < cells.size() && hIndex < hours.length; ++i) {
            Element cell = cells.get(i);
            String className = cell.className();
            if (className.contains("bus-routes")) {
                String routeID = parseAndCatCell(cell);
                if (routeID == null) {
                    // Log.d("Error", "系統番号なし");
                    return null;
                } else {
                    route.setRouteID(routeID);
                }
            } else if (className.contains("destination")) {
                Element child = cells.select("span").first();
                if (child == null) {
                    // Log.d("Error", "行先なし");
                    return null;
                }
                String dest = parseAndCatCell(child);
                if (dest == null) {
                    // Log.d("Error", "行先なし");
                    return null;
                } else {
                    route.setDest(dest);
                }
            } else {
                if (route.isValid() == false) {
                    return null;
                } else {
                    String[] minutes = parseCell(cell);
                    BusRoute.Time tm = route.getTime(hours[hIndex]);
                    tm.setMinutes(minutes);
                    hIndex++;
                }
            }
        }
        return route;
    }

    public BusTimeTable parse(Element elem) {
        BusTimeTable result = new BusTimeTable();
        Elements elements = elem.select("caption");
        if (elements.isEmpty()) {
            return null;
        }
        result.setCaption(elements.get(0).text());
        elements = elem.select("tr");
        if (elements.size() < 2) {
            return null;
        }
        int[] hours = parseBusTimeHours(elements.get(0));
        if (hours == null) {
            return null;
        }
        int numRoute = 0;
        BusRoute[] routes = new BusRoute[elements.size() - 1];
        for (int i = 1; i < elements.size(); ++i) {
            routes[i - 1] = parseRoute(elements.get(i), hours);
            if (routes[i - 1] != null) {
                numRoute++;
            }
        }
        result.alloc(numRoute);
        for (int i = 0, j = 0; i < routes.length; ++i) {
            if (routes[i] != null) {
                result.setRoute(j++, routes[i]);
            }
        }
        return result;
    }
}
