package com.example.portalapp.serviceBackground;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.example.portalapp.R;
import com.example.portalapp.campusInsideSite.PortalListActivity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

public class Notifier extends BroadcastReceiver {
	public  final String TAG = Notifier.class.getName();
	
	@Override
	public void onReceive(Context content, Intent intent) {
		
		//通知がクリックされた時に発行されるIntentの生成
		Intent sendIntent = new Intent(content, PortalListActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(content, 0, sendIntent, 0);

		//通知オブジェクトの生成
		NotificationCompat.Builder builder = new NotificationCompat.Builder(content)
			.setTicker(intent.getStringExtra("title"))
			.setContentTitle(intent.getStringExtra("title"))
			.setContentText(intent.getByteExtra("count", (byte)0)+"件の新着通知")
			.setVibrate(new long[]{0, 450, 100, 100, 100, 100})
			.setAutoCancel(true)
			.setSmallIcon(R.drawable.ic_oit_logo3)
			.setContentIntent(pendingIntent);
		if (isInclude("[重要]", intent.getStringExtra("title"))) {
			builder.setLargeIcon(BitmapFactory.decodeResource(content.getResources(), R.drawable.la_important));
		} else if (isInclude("[緊急]", intent.getStringExtra("title"))) {
			builder.setLargeIcon(BitmapFactory.decodeResource(content.getResources(), R.drawable.la_emergency));
		} else {
			if(intent.getStringExtra("type").equals("message")){
				builder.setLargeIcon(BitmapFactory.decodeResource(content.getResources(), R.drawable.la_message2));
			}else{
				builder.setLargeIcon(BitmapFactory.decodeResource(content.getResources(), R.drawable.la_graduation));
			}
		}
		NotificationCompat.BigTextStyle noti = new NotificationCompat.BigTextStyle(builder)
			.setBigContentTitle(intent.getStringExtra("title"))
			.bigText(intent.getStringExtra("org")
				+"\n"+intent.getStringExtra("date")
				+"\n\n"+intent.getStringExtra("detail"))
			.setSummaryText(intent.getByteExtra("count", (byte)0)+"件の新着通知");
		
		NotificationManager manager = (NotificationManager)content.getSystemService(Context.NOTIFICATION_SERVICE);
		manager.notify(0, noti.build());

	}
	
	/**
	 * 
	 * @param regexp
	 * @param text
	 * @return
	 */
	private boolean isInclude(String regexp, String text){
		Pattern p = Pattern.compile(".*("+regexp+"){1}.*");
		Matcher m = p.matcher(text);
		
		return m.find();
	}

}