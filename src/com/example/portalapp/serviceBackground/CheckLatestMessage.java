package com.example.portalapp.serviceBackground;
 
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.jsoup.nodes.Document;

import com.example.portalapp.R;
import com.example.portalapp.campusInsideSite.listInfo.HtmlHash;
import com.example.portalapp.campusInsideSite.listInfo.HtmlHash_Detail;
import com.example.portalapp.campusInsideSite.extJsoup.JsoupLogin;
import com.example.portalapp.campusInsideSite.extJsoup.JsoupParse;
import com.example.portalapp.campusInsideSite.extJsoup.JsoupParse_Detail;
import com.example.portalapp.library.SP;
import com.example.portalapp.settings.notifiinfo.NotifiInfoActivity;
import com.example.portalapp.library.sqlite.ReadDB;


import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;
 
// ______________________________________________________________________________
// サービス(バックグラウンドで実行される処理)
public class CheckLatestMessage extends Service {
	private final static short DEFAULT_LOAD_NUMBER = 1; // １ページ（１０項目）の確認だけでいい
	private final static String SERVICE_NAME = "CheckLatestMessage";
    private final static String TAG = "CheckLatestMessage#";
	private String SharedPreferenceFileName;
	private String isServiceNotificationKey;
	private String LatestNewsTimeValue;
    private SP sp;
    
    // ログイン情報にアクセスするためのクラス（読み込み）
	private ReadDB loginInfo;
     
    // Toastを何回表示されたか数えるためのカウント
    //private int mCount = 0;
    // Toastを表示させるために使うハンドラ
    private Handler handler = new Handler();
    // スレッドを停止するために必要
    private boolean PortalAccessThreadActive = true;

    private int errCnt = 0; private boolean isTaskKill = false;
     
    // スレッド処理
    private Runnable PortalAccessTask = new Runnable() {
         
        @Override
        public void run() {
             
            // アクティブな間だけ処理をする
            while (PortalAccessThreadActive) {
                //mCount++;
                
                try {
                    Thread.sleep(700);
                } catch (InterruptedException e) {
                    // TODO 自動生成された catch ブロック
                    e.printStackTrace();
                }
                
                if(!(errCnt > 3 || isTaskKill)){
                
                	// 新着通知数のカウント
	                byte PostNotificationNum = 0;
					
	                // 読み込みページ数
					final short checkPageNum = DEFAULT_LOAD_NUMBER;
					Document doc = null;
					HtmlHash[] hash = new HtmlHash[10];
					try {
			        	loginInfo.init(); // 名前とパスワードをDBから読み込み
						
						JsoupLogin obj = new JsoupLogin();
						JsoupParse parse = new JsoupParse();
						obj.setInfo(loginInfo.getUsername(), loginInfo.getPassword());
						obj.login();
						
						for(short accessPageNum=0; accessPageNum<checkPageNum; accessPageNum++){
							doc = obj.getPortalPage(accessPageNum);
							parse.setDoc(doc);
							parse.doParse();
							hash = parse.getHash();
							
							if(sp.readStr(LatestNewsTimeValue) == ""){
								// セーブデータが無いときに実行
								sp.write(LatestNewsTimeValue,
										hash[0].date.replaceAll(" Date : ", "").replaceAll("\n", ""));
								
							}
							
							// 新着通知数を取得
			            	PostNotificationNum = getNewNotificationNum(hash);
			            	
			            	
						}
	
						if(PostNotificationNum != 0){
							// 新着通知がある場合に実行
				        	JsoupParse_Detail parse_detail = new JsoupParse_Detail();
				        	HtmlHash_Detail detail_hash = new HtmlHash_Detail();
				        	
				        	parse_detail.setDoc(obj.getDetailPage(doc, 0));
				        	parse_detail.doParse();
				        	detail_hash = parse_detail.getHash();
							
	                    	PostNotification(PostNotificationNum, hash, detail_hash);
	                    	
						}

		            	// セーブデータ（最新更新日時）の更新
		            	setLatestNewsTime(hash);
						
						isTaskKill = true;
					}catch(Exception e) {
						errCnt++;
						
					}
					
					// Serviceを終了する手続きに入る
	                if(errCnt > 3 || isTaskKill) stopSelf();
                
                }
                
                handler.post(new Runnable() {
                     
                    @Override
                    public void run() {
                        //showText("カウント:" + mCount);
                        	
                    }
                });
            }
             
            handler.post(new Runnable() {
                 
                @Override
                public void run() {
                    showText("スレッド終了");
                }
            });
        }
    };
    private Thread PortalAccessThread;
     
    // ______________________________________________________________________________
    /**
     * テキストを表示する
     * @param text 表示したいテキスト
     */
    private void showText(Context ctx, final String text) {
        //Toast.makeText(this, TAG + text, Toast.LENGTH_SHORT).show();
    	
    }
 
    // ______________________________________________________________________________
    /**
     * テキストを表示する
     * @param text テキスト
     */
    private void showText(final String text) {
        showText(this, text);
    }
             
             
    // ______________________________________________________________________________
    @Override   // onBind:サービスがバインドされたときに呼び出される
    public IBinder onBind(Intent intent) {
        this.showText("サービスがバインドされました。");
        return null;
    }
     
    // ______________________________________________________________________________
    @Override   // onStartCommand:
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
         
        this.showText("onStartCommand");
        this.PortalAccessThread = new Thread(null, PortalAccessTask, "NortifyingService");
        this.PortalAccessThread.start(); 

        if(sp.readBool(isServiceNotificationKey)){
        	// 通知バーを表示する
        	showNotification(this);
        }
         
        // 戻り値でサービスが強制終了されたときの挙動が変わる
        // START_NOT_STICKY,START_REDELIVER_INTENT,START_STICKY_COMPATIBILITY
        return START_STICKY;
    }
 
    // ______________________________________________________________________________
    @Override   // onCreate:サービスが作成されたときに呼びされる(最初に1回だけ)
    public void onCreate() {
        this.showText("サービスが開始しました。");
        super.onCreate();

    	SharedPreferenceFileName = getString(R.string.shared_preferences_file_name_notifi);
    	isServiceNotificationKey = getString(R.string.shared_preferences_notifi_tag_name_isServiceNotification);
    	LatestNewsTimeValue = getString(R.string.shared_preferences_notifi_tag_name_LatestNewsTimeValue);
        
        sp = new SP(SharedPreferenceFileName, this);

		loginInfo = new ReadDB(getApplicationContext());
    }
 
 
    // ______________________________________________________________________________
    @Override   // onDestroy:
    public void onDestroy() {
        this.showText("サービスが終了しました。");
         
        // スレッド停止
        this.PortalAccessThread.interrupt();
        this.PortalAccessThreadActive = false;
         
        if(sp.readBool(isServiceNotificationKey)){
        	// 通知バーを消す
        	this.stopNotification(this);
        }
        super.onDestroy();
    }
 
    // ______________________________________________________________________________
    // 通知バーを消す
    private void stopNotification(final Context ctx) {
        NotificationManager mgr = (NotificationManager)ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        mgr.cancel(R.layout.activity_main);
    }
     
    // ______________________________________________________________________________
    // 通知バーを出す
    private void showNotification(final Context ctx) {
         
        NotificationManager mgr = (NotificationManager)ctx.getSystemService(Context.NOTIFICATION_SERVICE);
         
        Intent intent = new Intent(ctx, NotifiInfoActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, 0);
 
        // 通知バーの内容を定義する
        Notification n = new NotificationCompat.Builder(ctx)
            .setSmallIcon(R.drawable.ic_oit_logo3)
            .setTicker("サービス（"+SERVICE_NAME+"）が起動しました")
            .setWhen(System.currentTimeMillis())
            .setContentTitle("サービス起動中")
            .setContentText("サービスがバックグラウンドで起動しています")
            //.setVibrate(new long[]{0, 100, 300, 300, 100, 100, 100, 100})
            .setContentIntent(contentIntent)
            .build();
        n.flags = Notification.FLAG_NO_CLEAR;
         
        mgr.notify(R.layout.activity_main, n);
         
    }
    
    // ______________________________________________________________________________
    /**
     * SharedPreferencesから保存された文字列の日時を
     * Timestampに変換する
     * @return Timestamp : タイムスタンプ
     */
    @SuppressLint("SimpleDateFormat") private Timestamp readSP_Timestamp(){
		SP sp = new SP(SharedPreferenceFileName, this);
    	Timestamp saved_timestamp = null;
    	try {
    		saved_timestamp = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm ")
				.parse(sp.readStr(LatestNewsTimeValue)).getTime());
    			
		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
    	return saved_timestamp;
    }
    
    /**
     * 新着通知数を返す
     * @param hash
     * @return byte : 新着通知数
     * @throws ParseException
     */
    @SuppressLint("SimpleDateFormat") private byte getNewNotificationNum(HtmlHash[] hash) throws ParseException{
		boolean isFinishCheck = false;
		final Timestamp saved_timestamp = readSP_Timestamp();
		byte PostNotificationNum = 0;
    	byte i = 0;
		while(!isFinishCheck && i < 10){
			String loaded_latest_date_time = hash[i].date.replaceAll(" Date : ", "").replaceAll("\n", "");
			Timestamp loaded_timestamp = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm ")
				.parse(loaded_latest_date_time).getTime());
			
			if(saved_timestamp.compareTo(loaded_timestamp) > 0){
				// ------------------------ 保存されているスタンプの方が最近の時 ------------------------
				isFinishCheck = true;
			}else if(saved_timestamp.compareTo(loaded_timestamp) == 0){
				// ------------------------ 保存されているスタンプと読み込んだスタンプが同じ時 ------------------------
				isFinishCheck = true;
			}else{
				// ------------------------ 読み込んだスタンプの方が最近の時 ------------------------
				// ------------------------ 新しい通知情報がある時 ------------------------
				PostNotificationNum++;
			}
			
			++i;
		}
		return (PostNotificationNum);
    }
    
    /**
	 * hash内の最も新しいデータの日時をSharedPreferencesに登録
	 * @param hash
	 * @throws ParseException
	 */
	@SuppressLint("SimpleDateFormat") private void setLatestNewsTime(HtmlHash[] hash) throws ParseException{
			SP sp = new SP(SharedPreferenceFileName, this);
			String saved_latest_date_time = sp.readStr(LatestNewsTimeValue);
			String loaded_latest_date_time = hash[0].date.replaceAll(" Date : ", "").replaceAll("\n", "");
			
			if(saved_latest_date_time != ""){
				// -------------------- セーブデータがある時 --------------------
				Timestamp saved_timestamp = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm ")
														.parse(saved_latest_date_time).getTime());
				Timestamp loaded_timestamp = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm ")
														.parse(loaded_latest_date_time).getTime());
				
				if(saved_timestamp.compareTo(loaded_timestamp) > 0){
					// 保存されているスタンプの方が最近の時
					//Log.i("now comp timestamp", saved_timestamp+", saved_timestampの方が遅い");
				}else if(saved_timestamp.compareTo(loaded_timestamp) == 0){
					// 保存されているスタンプと読み込んだスタンプが同じ時
				}else{
					// 読み込んだスタンプの方が最近の時
					//Log.i("now comp timestamp", saved_timestamp+", saved_timestampの方が早い");
					sp.write(LatestNewsTimeValue, loaded_latest_date_time);
				}
				
			}else{
				// -------------------- セーブデータがない時 --------------------
				sp.write(LatestNewsTimeValue, loaded_latest_date_time);
				
			}
			
			
	}

    /**
     * 新着の通知のうち最新の情報を通知するインテントを投げる
     * @param notifi_num 新着通知数
     * @param hash 通知データ１ページ分（１０個のリストデータ）
     */
    private void PostNotification(byte notifi_num, HtmlHash[] hash, HtmlHash_Detail detail_hash){
    	//呼び出す日時を設定する
		Calendar triggerTime = Calendar.getInstance();
		triggerTime.add(Calendar.SECOND, 0); //今から0秒後
		
		//設定した日時で発行するIntentを生成
		Intent intent = new Intent(CheckLatestMessage.this, Notifier.class);
		intent.putExtra("count", notifi_num);
		intent.putExtra("date", hash[0].date);
		intent.putExtra("title", hash[0].title);
		intent.putExtra("org", hash[0].org);
		intent.putExtra("type", hash[0].type);
		intent.putExtra("detail", detail_hash.detail);
		PendingIntent sender = PendingIntent.getBroadcast(
				CheckLatestMessage.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		AlarmManager manager = (AlarmManager)getSystemService(ALARM_SERVICE);
		manager.set(AlarmManager.RTC_WAKEUP, triggerTime.getTimeInMillis(), sender);
    }
    
     
}
