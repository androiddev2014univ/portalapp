package com.example.portalapp;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/*
 * ----------------------------------------------------------
 * dates					：	日付の文字列(String)
 * titles					：	項目の文字列(String)
 * msgs						：	メッセージ（内容）の文字列(String)
 * links					：	リンクの文字列(String)
 * 
 * kategories				：	項目ごとの番号(int)
 * ----------------------------------------------------------
 * 
 * 初期化関数					：	init()
 * 
 * リスト数取得関数				：	get_list_num()
 * 全データ取得関数（確認用）		：	get()
 * 取得関数					：	get_取得したい変数名()
 * ----------------------------------------------------------
 * 
 */
public class OitNewsLatest {
	final static String oit = "https://www.oit.ac.jp";
	final static String uri = oit + "/japanese/news/latest.php";
	final static String query = "?type=2";
	Document doc;
	Element elm;
	Elements elms_date, elms_title, elms_msg;
	private String[] dates, titles, msgs, links;
	private int[] kategories;
	private int cnt = 0;
	String res = "";
	
	public void init() throws IOException{
		doc = Jsoup.connect(uri + query).get();
		elm = doc.getElementById("news_box2");
		elms_date = elm.getElementsByClass("news_date");
		elms_title = elm.getElementsByTag("img");
		elms_msg = elm.getElementsByTag("a");
		
		this.cnt = elms_date.size();
		
		dates = new String[cnt];
		for(int i=0;i<cnt;i++){
			dates[i] = "20" + elms_date.get(i).ownText().replace(".", "/"); // 2099年まで
			res += dates[i] + "\n";
			
		}
		
		titles = new String[cnt];
		for(int i=0;i<cnt;i++){
			titles[i] = elms_title.get(i).attr("title");
			res += titles[i] + "\n";
			
		}
		
		kategories = new int[cnt];
		for(int i=0;i<cnt;i++){
			if(titles[i].equals("重要"))
				kategories[i] = 0;
			else if(titles[i].equals("トピックス"))
				kategories[i] = 1;
			else if(titles[i].equals("お知らせ"))
				kategories[i] = 2;
			else if(titles[i].equals("イベント"))
				kategories[i] = 3;
			else if(titles[i].equals("募集"))
				kategories[i] = 4;
			else // その他
				kategories[i] = -1;
			res += kategories[i] + "\n";
			
		}
		
		msgs = new String[cnt];
		for(int i=0;i<cnt;i++){
			msgs[i] = elms_msg.get(i).ownText();
			res += msgs[i] + "\n";
			
		}
		
		links = new String[cnt];
		for(int i=0;i<cnt;i++){
			links[i] = elms_msg.get(i).attr("href");
			if(links[i].startsWith("/japanese/")){
				links[i] = oit + links[i];
			}
			res += links[i] + "\n";
			
		}
		
	}
	public int get_list_num(){
		return this.cnt;
	}
	public String get(){
		return res;
	}
	public String[] get_dates(){
		return dates;
	}
	public String[] get_titles(){
		return titles;
	}
	public int[] get_kategories(){
		return kategories;
	}
	public String[] get_msgs(){
		return msgs;
	}
	public String[] get_links(){
		return links;
	}

	public String get_date(int i){
		return dates[i];
	}
	public String get_title(int i){
		return titles[i];
	}
	public int get_kategorie(int i){
		return kategories[i];
	}
	public String get_msg(int i){
		return msgs[i];
	}
	public String get_link(int i){
		return links[i];
	}
}
