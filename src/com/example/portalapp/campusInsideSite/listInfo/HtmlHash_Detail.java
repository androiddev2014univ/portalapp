package com.example.portalapp.campusInsideSite.listInfo;

public class HtmlHash_Detail extends HtmlHash {
	public String detail;
	public String check_date;

	public HtmlHash_Detail(
			String title, String date, String org, String type, String url,
			String detail, String check_date
	) {
		super(title, date, org, type, url);
		// TODO 自動生成されたコンストラクター・スタブ
		this.detail = detail;
		this.check_date = check_date;
		
	}
	public HtmlHash_Detail() {
		// TODO 自動生成されたコンストラクター・スタブ
	}
	public void set(
			String title, String date, String org, String type, String url,
			String detail, String check_date
	) {
		super.set(title, date, org, type, url);
		this.detail = detail;
		this.check_date = check_date;
	}

}
