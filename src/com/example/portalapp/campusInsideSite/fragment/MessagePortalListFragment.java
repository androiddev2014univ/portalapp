package com.example.portalapp.campusInsideSite.fragment;


import com.example.portalapp.R;
import com.example.portalapp.campusInsideSite.adapter.SiteNewsListAdapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class MessagePortalListFragment extends Fragment {

    public ListView mListView;
    private SiteNewsListAdapter mAdapter;
    private Activity parentActivity;
    private OnItemClickListener mItemClickListener;

    private static MessagePortalListFragment messagePortalListFragment;
    public static MessagePortalListFragment getInstance() {
        if (messagePortalListFragment == null) {
            messagePortalListFragment = new MessagePortalListFragment();
        }
        return messagePortalListFragment;
    }
    
    private MessagePortalListFragment() {
        
    }
    
    @SuppressLint("InflateParams") @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.message_portal_list_fragment, null);
    }

    @Override
    public void onStart() {
        super.onStart();
        parentActivity = getActivity();
        mListView = (ListView) parentActivity.findViewById(R.id.message_portal_list);
        mListView.setOnItemClickListener(mItemClickListener);
        
    }

    public void setAdapter(SiteNewsListAdapter adapter) {
        mAdapter = adapter;
        mListView.setAdapter(mAdapter);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mItemClickListener = listener;
    }

}
