package com.example.portalapp.campusInsideSite;

import com.example.portalapp.R;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

public class PortalDetailDialog extends DialogFragment {
    
    private Dialog dialog;
    private TextView titleTextView, dateTextView, detailTextView, checkDateTextView;
    private String title, date, detailText, check_date;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        dialog = new Dialog(getActivity());
        // タイトル非表示
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        // フルスクリーン
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        dialog.setContentView(R.layout.portal_detail_dialog);
        
        dialog.findViewById(R.id.close).setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				// TODO 自動生成されたメソッド・スタブ
				dismiss();
			}   	
        });
        dialog.findViewById(R.id.share).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO 自動生成されたメソッド・スタブ
				Intent intent = new Intent(android.content.Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_TEXT, detailText);
				startActivity(intent);
			}
		});
        dialog.setCanceledOnTouchOutside(false);
        
        return dialog;
    }
    
    @Override
    public void onStart() {
        super.onStart();

        /* view の初期化はonStartで */
        titleTextView = (TextView) dialog.findViewById(R.id.title);
        dateTextView = (TextView) dialog.findViewById(R.id.date);
        detailTextView = (TextView) dialog.findViewById(R.id.detail_text);
        checkDateTextView = (TextView) dialog.findViewById(R.id.check_date);
        //titleTextView.setText(this.title);
        dateTextView.setText(this.date);
        detailTextView.setText(detailText);
        checkDateTextView.setText(this.check_date);
        
        // リンクを付加する
        Linkify.addLinks(detailTextView, Linkify.ALL);
        
    }
    
    public void setDetailText(String title, String date, String text, String check_date) {
        detailText = text;
        this.title = title.replaceAll("\t", " ").replaceAll("\n", "　　　　　");
        this.date = date;
        this.check_date = check_date;
    }

    @Override
    public void onStop(){
    	super.onStop();
    	//Log.d("onStop", "Stopped!!");
    	dialog.dismiss();
    	
    }
    
    @Override
    public void onPause(){
    	super.onPause();
    	titleTextView.setText("");
    	//Log.d("onPause", "paused!!");
    }
    
    @Override
    public void onResume(){
    	super.onResume();
    	titleTextView.setText(this.title);
    	//Log.d("onResume", "resumed!!");
    	
    }
    
}
