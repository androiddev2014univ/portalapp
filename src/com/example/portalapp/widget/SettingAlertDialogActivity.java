package com.example.portalapp.widget;

import com.example.portalapp.R;
import com.example.portalapp.library.SP;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;

// このアクテビティの背景はstyle.xmlの設定によって透過されています
public class SettingAlertDialogActivity extends FragmentActivity {
	private String SharedPreferenceFileName;
	private String typeSignalTagKey;
	private SP sp;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        SharedPreferenceFileName = getString(R.string.shared_preferences_file_name_bustime);
        typeSignalTagKey = getString(R.string.shared_preferences_bustime_tag_name_typeSignal);
		
        sp = new SP(SharedPreferenceFileName, this);
		
        SettingAlertDialogFragment fragment = new SettingAlertDialogFragment();
        fragment.show(getSupportFragmentManager(), "alert_dialog");
        
    }
    
    public class SettingAlertDialogFragment extends DialogFragment {
    	
        private int singleChoicePosition = 0;
        @Override
    	public Dialog onCreateDialog(Bundle savedInstanceState){
    		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_DARK);
    		CharSequence[] array = getResources().getStringArray(R.array.direction_names);
    		singleChoicePosition = 0;
    		
    		builder.setTitle("時刻表の種類を選択")
    		.setSingleChoiceItems(array, sp.readInt(typeSignalTagKey), new DialogInterface.OnClickListener() {
    			@Override
    			public void onClick(DialogInterface dialog, int which) {
    				singleChoicePosition = which;
    			}
    		});
    		builder.setPositiveButton("Set", new DialogInterface.OnClickListener() {
    			@Override
    			public void onClick(DialogInterface dialog, int which) {
    				/*Toast.makeText(getActivity(),
    						"OK Clicked! singleChoicePosition:"+singleChoicePosition, Toast.LENGTH_SHORT
    						).show();*/
    				
    				sp.write(typeSignalTagKey, singleChoicePosition);
    				
    			}
    		});
    		builder.setNegativeButton("Cancel", null);
    		Dialog dialog = builder.create();
    		dialog.setCanceledOnTouchOutside(false);
    		
    		return dialog;
    	}
     
        @Override
        public void onStop() {
            super.onStop();
            // Log.v("onStopLog", "Stopped");
            
        }
        
        @Override
        public void onDestroy() {
            super.onDestroy();
            // Log.v("onDestroy", "Destroyed");
            
            getActivity().finish();
        }
        
    }
    
}
