package com.example.portalapp.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;

public class KeihanBusWidgetProvider extends AppWidgetProvider {
	
	@Override
	public void onEnabled(Context context) {
		super.onEnabled(context);
	}
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		// サービスの起動
		Intent intent = new Intent(context, BusTimeTableReloadService.class);
		context.startService(intent);
		
	}
	
	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		super.onDeleted(context, appWidgetIds);
		// サービスの終了
		Intent intent = new Intent(context, BusTimeTableReloadService.class);
		context.stopService(intent);
		
	}
	
	@Override
	public void onDisabled(Context context) {
		super.onDisabled(context);
		
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);
		
	}

}
